# TLYZGradeSearch

#### Description
This is TLYZGradeSearch,A simple Grade Searching Program!It's a console program based on C++.It's only purpose is to search for Grades!  
There's no other purposes till now.

#### Software Architecture
Only Support x86 and x86_64 Architecture  

#### Installation
1. **for Linux**  
   
    -  Install git,cmake,make,gcc,g++  
    
      For Debian Linux
    ```bash
    sudo apt-get install git cmake make gcc g++ -y  
    ```
    Other LInux: Please Help yourself!(HAHA)
    
    - Execute codes downside
    
    ```bash
    git clone https://gitee.com/jerrypaullee/TLYZGradeSearch.git  
    cd TLYZGradeSearch  
    cmake .
    make
    ```
    - Now,the Executable File **TLYZGradeSearch** should be on your directory!
    
2. **for Windows**
   
    - Method One（Using **CMake for Windows** and **MinGW Make**)
        1. Go to [MinGW Offical Website](mingw.org) and [CMake Ofiical Website](cmake.org) Download and install **MinGW** and **CMake for Windows**.(Also,you can use [TDM-GCC](https://jmeubank.github.io/tdm-gcc/) or [MinGW-w64](http://www.mingw-w64.org/).)  
        
        2. Go to [Git Ofiical Website](git-scm.com) download and install **Git For Windows**。  
        
        3. Open CMD or Powershell,Execute:
           ```cmd
           git clone https://gitee.com/jerrypaullee/TLYZGradeSearch.git
           cd TLYZGradeSearch
           cmake -G "MinGW Makefile"
           mingw32-make
           ```
           
        4. Now,**TLYZGradeSearch.exe** should in your directory.
        
    - Method Two(Using **Visual Studio 2019**)(Working)

#### Instructions
It's very Simple and you only need to follow the Software Instruction.

Oops!The Software don't support English!

#### Contribution

1.  Fork the repository
2.  Create (Your Name) branch
3.  Commit your code
4.  Create Pull Request

