# 铜陵一中查分程序

Looking for English Version?[Click Here!](https://gitee.com/jerrypaullee/TLYZGradeSearch/blob/dev/README.en.md)

#### 介绍
铜陵一中查分程序(TLYZGradeSearch)是基于C++的一款小型的控制台应用程序，就是做出来用来查分滴！哈哈  
目前来看。。。还木有其他用途

#### 软件架构
目前只支持x86和x86-64架构


#### 构建教程
1. **for Linux**  
   
    - 安装git,cmake,make,gcc,g++  
    对于Debian系Linux发行版:  
    ```bash
    sudo apt-get install git cmake make gcc g++ -y  
    ```
    对于其他发行版：请自行想办法（哈哈）
    
    - 执行以下命令  
    ```bash
    git clone https://gitee.com/jerrypaullee/TLYZGradeSearch.git  
    cd TLYZGradeSearch  
    cmake .
    make
    ```
    - 此时，可执行文件**TLYZGradeSearch**应该出现在你的目录里啦！QAQ!
2. **for Windows**
   
    - 方法一：(使用**CMake for Windows**和**MinGW Make**)
        1. 到[MinGW官网](mingw.org)和[CMake官网](cmake.org)下载安装**MinGW**和**CMake for Windows**。（当然，你用[TDM-GCC](https://jmeubank.github.io/tdm-gcc/)或者[MinGW-w64](http://www.mingw-w64.org/)也行（2333））  
        
        2. 到[Git官网](git-scm.com)下载安装**Git For Windows**。  
        
        3. 打开CMD或Powershell,执行:
           ```cmd
           git clone https://gitee.com/jerrypaullee/TLYZGradeSearch.git
           cd TLYZGradeSearch
           cmake -G "MinGW Makefile"
           mingw32-make
           ```
           
        4. 此时**TLYZGradeSearch.exe**应该出现在目录里了。
        
    - 方法二：（使用**Visual Studio**）（敬请期待）

#### 使用说明

这个软件很简单，直接跟着说明走就行啦！

#### 参与贡献

1.  Fork 本仓库
2.  新建 （你的名字） 分支
3.  提交代码
4.  新建 Pull Request

